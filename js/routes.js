var routes = [     
    {path: "/", url: "index.html"},       
    {path: "/tambah/", componentUrl: "pages/tambah.html"},  
    {path: "/scan/", componentUrl: "pages/scan.html"},
    {path: "/scan-kasir/", componentUrl: "pages/scan-kasir.html"},
    {path: "/produk/", componentUrl: "pages/produk.html"},
    {path: "/login/", componentUrl: "pages/login.html"},  
    {path: "/about/", componentUrl: "pages/about.html"},
    {path: "(.*)", url: "pages/404.html"} 
]; 
     